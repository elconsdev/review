module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: 'tsconfig.json',
    tsconfigRootDir: __dirname,
    sourceType: 'module',
  },
  plugins: ['@typescript-eslint/eslint-plugin'],
  extends: [
    'plugin:@typescript-eslint/recommended',
  ],
  root: true,
  env: {
    node: true,
    jest: true,
  },
  ignorePatterns: ['.eslintrc.js'],
  rules: {
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    'semi': [2, 'always'],
    'linebreak-style': [0],
    'quotes': [2, 'single'],
    'object-curly-spacing': [2, 'never'],
    'arrow-parens': 'off',
    'indent': [2, 4, {
      'SwitchCase': 1,
      'FunctionDeclaration': {'parameters': 'first'},
      'FunctionExpression': {'parameters': 'first'},
      'flatTernaryExpressions': true,
      'ignoredNodes': ['ConditionalExpression', 'PropertyDefinition']
    }]
  },
};
