# review
1. Create .env file with defaults:

- `APP_PORT=3000`
- `DATABASE_URL="file:./dev.db"`
- `ADMIN_EMAIL=<admin email>`
- `ADMIN_PASSWORD=<admin password>`
2. Run `npm install`
3. Run `npm start`
4. Open `localhost:3000/api/doc`
