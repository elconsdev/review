import {Body, Controller, Post, Req, UseGuards} from '@nestjs/common';
import {AuthService, LoginResult} from './auth.service';
import {LoginDto} from './dto/login.dto';
import {ApiBearerAuth, ApiOperation, ApiResponse, ApiTags} from '@nestjs/swagger';
import {AuthGuard} from '../guards/auth.guard';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService
    ) {}

    @ApiOperation({summary: 'Login user'})
    @ApiResponse({status: 200})
    @Post('/login')
    async login(@Body() dto: LoginDto): Promise<LoginResult> {
        return this.authService.login(dto);
    }

    @ApiBearerAuth('access-token')
    @ApiOperation({summary: 'Refresh token'})
    @ApiResponse({status: 200})
    @Post('/refresh')
    @UseGuards(AuthGuard)
    async refresh(@Req() req) {
        return this.authService.refreshUserToken(req.user?.id);
    }
}
