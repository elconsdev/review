import {IsNotEmpty, IsString} from 'class-validator';
import {ApiProperty} from '@nestjs/swagger';

export class LoginDto {
    @ApiProperty({description: 'user email'})
    @IsString()
    @IsNotEmpty()
    email: string;

    @ApiProperty({description: 'user password'})
    @IsString()
    @IsNotEmpty()
    password: string;
}

