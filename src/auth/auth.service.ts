import {BadRequestException, Injectable, Logger, NotFoundException, OnModuleInit} from '@nestjs/common';
import {LoginDto} from './dto/login.dto';
import {UserService} from '../user/user.service';
import * as JWT from 'jsonwebtoken';
import * as bcrypt from 'bcryptjs';
import {ConfigService} from '@nestjs/config';
import {AppConfig} from '../configs/app.config';
import {User} from '@prisma/client';
import {UserRole} from '../user/dto/createUser.dto';

@Injectable()
export class AuthService implements OnModuleInit {
    private readonly appConfig: AppConfig;
    constructor(
        private userService: UserService,
        private configService: ConfigService
    ) {
        this.appConfig = this.configService.get<AppConfig>('app');

    }
    async onModuleInit() {
        await this.checkAdmin();
    }

    private _generateToken (payload: TokenPayload) {
        return JWT.sign(payload, this.appConfig.jwtKey, {expiresIn: '8h'});
    }

    private getUserPayload (token: string): TokenPayload | null {
        return JWT.verify(token, this.appConfig.jwtKey) as TokenPayload;
    }
    private loginResult (user: User): LoginResult {
        return {
            token: this._generateToken({userId: user.id}),
            role: user.role
        };
    }

    async refreshUserToken (userId: number): Promise<LoginResult> {
        if (userId == null) {
            throw new BadRequestException('Missing userId');
        }
        const user = await this.userService.getById(userId);
        if (user == null) {
            throw new BadRequestException('User not found');
        }
        return this.loginResult(user);
    }


    public async login (dto: LoginDto) {
        const user = await this.userService.getByEmail(dto.email);
        if (user == null) {
            throw new NotFoundException('User not found');
        }
        if (!bcrypt.compareSync(dto.password, user.hash)) {
            throw new BadRequestException('Wrong password');
        }
        return this.loginResult(user);
    }

    async getUserByToken (token: string) {
        try {
            const payload = this.getUserPayload(token);
            return this.userService.getById(payload?.userId);
        } catch (e) {
            throw new BadRequestException(e.message);
        }
    }

    async checkAdmin () {
        Logger.debug('Check admin accounts');
        const admin = await this.userService.getByRole(UserRole.Admin);
        if (admin == null) {
            const {adminEmail, adminPassword} = this.appConfig;
            if (!adminEmail || !adminPassword) {
                Logger.error('Can not create admin account: credentials not defined');
                return;
            }
            await this.userService.create({
                email: adminEmail,
                password: adminPassword,
                role: UserRole.Admin
            });
            Logger.debug('Admin account has been created');
        }
    }
}
type TokenPayload = {
    userId: number;
}
export type LoginResult = {
    token: string;
    role: string;
}

