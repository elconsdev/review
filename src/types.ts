import {Prisma} from '@prisma/client';

export type Pagination = {
    page?: number;
    rowsPerPage?: number;
    orderBy?: Prisma.UserOrderByWithRelationInput;
    descending?: boolean
}
