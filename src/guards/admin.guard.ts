import {
    CanActivate,
    ExecutionContext,
    Inject,
    Injectable,
    UnauthorizedException
} from '@nestjs/common';
import {AuthService} from '../auth/auth.service';
import {UserRole} from '../user/dto/createUser.dto';

@Injectable()
export class AdminGuard implements CanActivate {
    constructor(
        @Inject(AuthService)
        private readonly authService: AuthService,
    ) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const req = context.switchToHttp().getRequest();
        if (req.user?.role !== UserRole.Admin) {
            throw new UnauthorizedException('Access denied');
        }
        return Promise.resolve(true);
    }
}
