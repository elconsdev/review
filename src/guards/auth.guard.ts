import {CanActivate, ExecutionContext, Inject, Injectable, UnauthorizedException} from '@nestjs/common';
import {AuthService} from '../auth/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(@Inject(AuthService) private readonly authService: AuthService) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const req = context.switchToHttp().getRequest();
        const bearerToken = req.headers?.authorization;
        const token = bearerToken != null ? bearerToken.split(' ')[1] : null;
        if (token == null) {
            throw new UnauthorizedException('Missing token');
        }
        const user = await this.authService.getUserByToken(token);
        if (user == null) {
            throw new UnauthorizedException('User not found');
        }
        req.user = {
            id: user.id,
            role: user.role
        };
        return true;
    }
}
