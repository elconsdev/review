import {BadRequestException, Injectable, NotFoundException} from '@nestjs/common';
import {PrismaService} from '../prisma.service';
import {Pagination} from '../types';
import {Prisma, Profile, User} from '@prisma/client';
import {CreateUserDto, UserRole} from './dto/createUser.dto';
import {UpdateUserDto} from './dto/updateUser.dto';
import * as bcrypt from 'bcryptjs';

const includedRelations: Prisma.UserInclude = {
    profile: true
};

@Injectable()
export class UserService {
    constructor(private prisma: PrismaService) {}

    private static getHash (password: string) {
        return bcrypt.hashSync(password, 12);
    }

    async getAll (params?: GetUserParams, relations = false) {
        const {page = 1, rowsPerPage = 10, where, orderBy} = params || {};
        const args: Prisma.UserFindManyArgs = {
            skip: (page - 1) * rowsPerPage,
            take: rowsPerPage,
            where,
            orderBy,
        };
        if (relations) {
            args.include = includedRelations;
        }
        return this.prisma.user.findMany({
            ...args,
            omit: {
                hash: true
            }
        });
    }

    async getById (id: number, relations = false): Promise<UserWithRelations> {
        const args: Prisma.UserFindUniqueArgs = {
            where: {id}
        };
        if (relations) {
            args.include = includedRelations;
        }
        return this.prisma.user.findUnique(args);
    }

    async getByEmail (email: string) {
        return this.prisma.user.findUnique({
            where: {email},
            include: {
                profile: true
            }
        });
    }

    async getByRole (role: UserRole) {
        return this.prisma.user.findFirst({where: {role}});
    }

    async create (dto: CreateUserDto): Promise<User> {
        const {email, password, firstName, lastName} = dto;
        const userCandidate = await this.getByEmail(email);
        if (userCandidate != null) {
            throw new BadRequestException('User already exist');
        }
        return this.prisma.user.create({
            data: {
                email,
                hash: password ? UserService.getHash(password) : null,
                role: dto.role || UserRole.User,
                profile: {
                    create: {
                        firstName,
                        lastName
                    }
                }
            }
        });
    }

    async update (id: number, dto: UpdateUserDto) {
        const user = await this.getById(id);
        if (user == null) {
            throw new NotFoundException('User not found');
        }
        const {firstName, lastName, password, role} = dto;

        await this.prisma.user.update({
            where: {id},
            data: {
                role,
                hash: password ? UserService.getHash(password) : undefined,
                profile: {
                    update: {
                        firstName,
                        lastName
                    }
                }
            }
        });

    }

    async remove (id: number) {
        const user = await this.getById(id);
        if (user == null) {
            throw new NotFoundException('User not found');
        }
        await this.prisma.user.delete({where: {id}});
        return {success: true};
    }

    async removeAll () {
        return this.prisma.user.deleteMany({});
    }
}
export type GetUserParams = Pagination & {
    where?: Prisma.UserWhereUniqueInput;
}
export type UserWithRelations = User & Partial<{
    profile: Profile;
}>
