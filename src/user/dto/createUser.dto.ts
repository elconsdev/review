import {ApiProperty} from '@nestjs/swagger';
import {IsNotEmpty, IsOptional, IsString} from 'class-validator';
export enum UserRole {
    User = 'user',
    Admin = 'admin'
}
export class CreateUserDto {
    @ApiProperty({description: 'User email'})
    @IsString()
    @IsNotEmpty()
    email: string;
    
    @ApiProperty({description: 'User password'})
    @IsString()
    @IsNotEmpty()
    password: string;

    @ApiProperty({description: 'User role'})
    @IsString()
    @IsOptional()
    role?: UserRole;

    @ApiProperty({description: 'First name'})
    @IsString()
    @IsOptional()
    firstName?: string;

    @ApiProperty({description: 'Last name'})
    @IsString()
    @IsOptional()
    lastName?: string;
}
