import {ApiProperty} from '@nestjs/swagger';
import {IsString, IsOptional} from 'class-validator';
import {UserRole} from './createUser.dto';

export class UpdateUserDto {
    @ApiProperty({description: 'User password'})
    @IsString()
    @IsOptional()
    password?: string;

    @ApiProperty({description: 'User role'})
    @IsString()
    @IsOptional()
    role?: UserRole;

    @ApiProperty({description: 'First name'})
    @IsString()
    @IsOptional()
    firstName?: string;

    @ApiProperty({description: 'Last name'})
    @IsString()
    @IsOptional()
    lastName?: string;
}
