import {Test, TestingModule} from '@nestjs/testing';
import {UserService, UserWithRelations} from './user.service';
import {PrismaService} from '../prisma.service';
import {CreateUserDto, UserRole} from './dto/createUser.dto';

const mockUserDto: CreateUserDto = {
    email: 'test@mail.com',
    password: '12345',
    firstName: 'test user first name',
    lastName: 'test user last name'
};

describe('UserService', () => {
    let service: UserService;
    let user: UserWithRelations;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [PrismaService, UserService],
        }).compile();

        service = module.get<UserService>(UserService);
        await service.removeAll();
    });

    beforeEach(async () => {
        //
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
    it('should create user', async () => {
        user = await service.create(mockUserDto);
        console.log(user);
        expect(user.email).toEqual(mockUserDto.email);
        expect(user.role).toEqual(UserRole.User);
    });
    it('get user should return user with profile', async () => {
        user = await service.getByEmail(mockUserDto.email);
        expect(user.profile).toBeDefined();
        expect(user.profile.firstName).toEqual(mockUserDto.firstName);
        expect(user.profile.lastName).toEqual(mockUserDto.lastName);
    });
    it('should thrown when create user with existing email', async () => {
        const createUser = async () => {
            await service.create(mockUserDto);
        };
        await expect(createUser).rejects.toThrow('User already exist');
    });
    it('remove user should not to thrown', async () => {
        const result = await service.remove(user.id);
        expect(result).toEqual({success: true});
    });
    it('user should not be returns after removing', async () => {
        user = await service.getByEmail(mockUserDto.email);
        expect(user).toBeNull();
    });
});
