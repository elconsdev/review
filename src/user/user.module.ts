import {Module} from '@nestjs/common';
import {UserService} from './user.service';
import {UserController} from './user.controller';
import {PrismaService} from '../prisma.service';

@Module({
    imports: [],
    providers: [PrismaService, UserService],
    exports: [UserService],
    controllers: [UserController]
})
export class UserModule {}
