import {Controller, Delete, Get, Param, Query, UseGuards} from '@nestjs/common';
import {ApiBearerAuth, ApiOperation, ApiQuery, ApiResponse, ApiTags} from '@nestjs/swagger';
import {UserService} from './user.service';
import {AdminGuard} from '../guards/admin.guard';
import {AuthGuard} from '../guards/auth.guard';
import {Prisma} from '@prisma/client';

@Controller('user')
@ApiTags('User')
export class UserController {
    constructor(private readonly userService: UserService) {}

    @Get()
    @ApiOperation({summary: 'Get all users'})
    @ApiBearerAuth('access-token')
    @ApiQuery({name: 'page', type: Number, required: false})
    @ApiQuery({name: 'rowsPerPAge', type: Number, required: false})
    @ApiQuery({name: 'orderBy', type: String, required: false})
    @ApiQuery({name: 'descending', type: Boolean, required: false})
    @ApiResponse({status: 200, type: String})
    @UseGuards(AuthGuard, AdminGuard)
    getAll (
        @Query('page') page: number,
        @Query('rowsPerPage') rowsPerPage: number,
        @Query('orderBy') orderBy: Prisma.UserOrderByWithRelationInput,
        @Query('descending') descending: boolean,
    ) {
        return this.userService.getAll({
            page, rowsPerPage, orderBy, descending
        }, true);
    }

    @Get(':id')
    @ApiOperation({summary: 'Get user by id'})
    @ApiBearerAuth('access-token')
    @UseGuards(AuthGuard, AdminGuard)
    @ApiResponse({status: 200, type: String})
    getById (@Param('id') id: string) {
        return this.userService.getById(Number(id));
    }

    @Delete(':id')
    @ApiOperation({summary: 'Remove user by id'})
    @ApiBearerAuth('access-token')
    @UseGuards(AuthGuard, AdminGuard)
    @ApiResponse({status: 200, type: String})
    remove (@Param('id') id: string) {
        return this.userService.remove(Number(id));
    }
}
