import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import * as fs from 'fs';
import {Logger, ValidationPipe} from '@nestjs/common';
import {ConfigService} from '@nestjs/config';
import {DocumentBuilder, SwaggerModule} from '@nestjs/swagger';

async function bootstrap() {
    const certPath = process.env.SSL_CERT_PATH;
    const keyPath = process.env.SSL_KEY_PATH;

    let httpsOptions;
    try {
        if (certPath != null && keyPath != null) {
            httpsOptions = {
                cert: fs.readFileSync(certPath).toString(),
                key: fs.readFileSync(keyPath).toString()
            };
        }
    } catch (e) {
        Logger.log(e);
    }
    const app = await NestFactory.create(AppModule, {httpsOptions});
    app.setGlobalPrefix('api');
    app.useGlobalPipes(new ValidationPipe());
    app.enableCors();

    const configService: ConfigService = app.get<ConfigService>(ConfigService);

    const config = new DocumentBuilder()
        .setTitle('API documentation')
        .setDescription('API documentation')
        .setVersion('1.0')
        .addBearerAuth(
            {type: 'http', scheme: 'bearer', bearerFormat: 'JWT', in: 'header'},
            'access-token',
        )
        .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api/doc', app, document);

    const port = configService.get('APP_PORT') || 3000;
    await app.listen(port);

    Logger.log(`Start listening on ${port}`);
}
bootstrap();
