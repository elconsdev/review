import {Module} from '@nestjs/common';
import {ConfigModule as NestConfigModule} from '@nestjs/config';
import {appConfig} from './configs/app.config';
import {UserService} from './user/user.service';
import {UserModule} from './user/user.module';
import {PrismaService} from './prisma.service';
import {AuthModule} from './auth/auth.module';

@Module({
    imports: [
        NestConfigModule.forRoot({
            envFilePath: ['.env', '../.env'],
            load: [appConfig],
            isGlobal: true
        }),
        UserModule,
        AuthModule
    ],
    controllers: [],
    providers: [PrismaService, UserService],
})
export class AppModule {}
