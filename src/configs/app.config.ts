import {registerAs} from '@nestjs/config';
import * as process from 'process';

const DEFAULT_JWT_KEY = 'jwt-key-g8rhg48gh4gh9w8gh-ew8hg-48h34g9w8gh9w8rgh48hg948gh4gh04gh84gh4g-04g04g48';
export interface AppConfig {
  port: number,
  jwtKey: string,
  adminEmail: string;
  adminPassword: string;
}

export const appConfig = registerAs(
    'app',
    (): AppConfig => ({
        port: parseInt(process.env.APP_PORT) || 3000,
        jwtKey: process.env.JWT_KEY || DEFAULT_JWT_KEY,
        adminEmail: process.env.ADMIN_EMAIL,
        adminPassword: process.env.ADMIN_PASSWORD
    }),
);
